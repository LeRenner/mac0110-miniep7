# MAC0110-MiniEP7

This is an exercise for the subject MAC110, part of the Computer Science degree at University of São Paulo. This specific exercise focuses on the calculation of sin(x), cos(x), and tan(x) using Taylor Series and the Julia Programming Language. It is also and introduction to version control using git.

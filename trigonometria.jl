#MAC110 - MiniEP7
#João Renner Rudge - 11276221

function bernoulli(n)
  n *= 2
  A = Vector{Rational{BigInt}}(undef, n + 1)
  for m = 0 : n
    A[m + 1] = 1 // (m + 1)
    for j = m : -1 : 1
      A[j] = j * (A[j] - A[j + 1])
    end
  end
  return abs(A[1])
end

function sin(x)
    sinal = true #true for positive number, false for negative number
    soma = 0
    potencia = x
    numeroFatorial = 1
    proximoFatorial = 2
    for i in 1:10
        if sinal
            soma += potencia / numeroFatorial
        else
            soma -= potencia / numeroFatorial
        end
        potencia *= (x ^ 2)
        numeroFatorial *= proximoFatorial
        numeroFatorial *= (proximoFatorial + 1)
        proximoFatorial += 2
        sinal = ~(sinal)
    end
    return round(soma, digits=3)
end

function cos(x)
    sinal = true #true for positive number, false for negative number
    soma = 0
    potencia = 1
    numeroFatorial = 1
    proximoFatorial = 1
    for i in 1:10
        if sinal
            soma += potencia / numeroFatorial
        else
            soma -= potencia / numeroFatorial
        end
        potencia *= (x ^ 2)
        numeroFatorial *= proximoFatorial
        numeroFatorial *= (proximoFatorial + 1)
        proximoFatorial += 2
        sinal = ~(sinal)
    end
    return round(soma, digits=3)
end

function tan(x)
    soma = 0
    for n in 1:10
        soma += ( 2^(2*n) * (2^(2*n) - 1) * bernoulli(n) * x^(2*n - 1) ) / ( factorial(2*n) )
    end
    soma = round(soma, digits=3)
    return trunc(Int, soma * 1000) / 1000
end

function check_sin(value, x)
    return abs(sin(x) - value) < 0.001
end

function check_cos(value, x)
    return abs(cos(x) - value) < 0.001
end

function check_tan(value, x)
    return abs(tan(x) - value) < 0.001
end

function taylor_sen(x)
    sinal = true #true for positive number, false for negative number
    soma = 0
    potencia = x
    numeroFatorial = 1
    proximoFatorial = 2
    for i in 1:10
        if sinal
            soma += potencia / numeroFatorial
        else
            soma -= potencia / numeroFatorial
        end
        potencia *= (x ^ 2)
        numeroFatorial *= proximoFatorial
        numeroFatorial *= (proximoFatorial + 1)
        proximoFatorial += 2
        sinal = ~(sinal)
    end
    return round(soma, digits=3)
end

function taylor_cos(x)
    sinal = true #true for positive number, false for negative number
    soma = 0
    potencia = 1
    numeroFatorial = 1
    proximoFatorial = 1
    for i in 1:10
        if sinal
            soma += potencia / numeroFatorial
        else
            soma -= potencia / numeroFatorial
        end
        potencia *= (x ^ 2)
        numeroFatorial *= proximoFatorial
        numeroFatorial *= (proximoFatorial + 1)
        proximoFatorial += 2
        sinal = ~(sinal)
    end
    return round(soma, digits=3)
end

function taylor_tan(x)
    soma = 0
    for n in 1:10
        soma += ( 2^(2*n) * (2^(2*n) - 1) * bernoulli(n) * x^(2*n - 1) ) / ( factorial(2*n) )
    end
    return trunc(Int, soma * 1000) / 1000
end

using Test
function test()
    @test sin(pi/3) ≈ sqrt(3)/2 atol = 0.001
    @test sin(pi/6) ≈ 0.5 atol = 0.001
    @test sin(0) ≈ 0 atol = 0.001
    @test sin(pi/2) ≈ 1 atol = 0.001

    @test cos(pi/3) ≈ 0.5 atol = 0.001
    @test cos(pi/6) ≈  sqrt(3)/2 atol = 0.001
    @test cos(0) ≈ 1 atol = 0.001
    @test cos(pi/2) ≈ 0 atol = 0.001

    @test tan(pi/3) ≈ sqrt(3) atol = 0.001
    @test tan(pi/6) ≈ sqrt(3)/3 atol = 0.001
    @test tan(0) ≈ 0 atol = 0.001


    @test check_sin(0.5, pi/6)
    @test check_sin(sqrt(3)/2, pi/3)
    @test check_sin(0, 0)
    @test check_sin(1, pi/2)

    @test check_cos(0.5, pi/3)
    @test check_cos(sqrt(3)/2, pi/6)
    @test check_cos(1, 0)
    @test check_cos(0, pi/2)

    @test check_tan(sqrt(3)/3, pi/6)
    @test check_tan(sqrt(3), pi/3)
    @test check_tan(0, 0)

    println("Final dos testes!")
end
